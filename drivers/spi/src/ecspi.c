/*
 * Copyright (c) 2011-2012, Freescale Semiconductor, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of Freescale Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#include "error_codes.h"

#include "core/ccm_pll.h"
#include "spi/ecspi_ifc.h"
#include "iomux_config.h"
#include "registers/regsecspi.h"
#include "registers/regsiomuxc.h" 
#include "timer.h"

#define ECSPI_FIFO_SIZE         64

#define SPI_RETRY_DEAY  50

static void ecspi_start_transfer(unsigned instance, uint16_t brs_bts);
static int ecspi_xfer_slv(unsigned instance, const uint8_t * tx_buf, uint8_t * rx_buf, int bytes);
static int ecspi_xfer_mst(unsigned instance, const uint8_t * tx_buf, uint8_t * rx_buf, int bytes);

////////////////////////////////////////////////////////////////////////////////
// Code
////////////////////////////////////////////////////////////////////////////////

static void ecspi_start_transfer(unsigned instance, uint16_t brs_bts)
{
    // Set burst length 
    HW_ECSPI_CONREG(instance).B.BURST_LENGTH = brs_bts - 1;

    // Clear status 
    HW_ECSPI_STATREG_WR(instance, BM_ECSPI_STATREG_RO | BM_ECSPI_STATREG_TC);
}

static int ecspi_xfer_slv(unsigned instance, const uint8_t * tx_buf, uint8_t * rx_buf, int bytes)
{
    printf("Slave mode transfer code not implemented yet.\n");
    return ERROR_GENERIC;
}

//! @brief Perform a SPI master transfer.
//!
//! @param instance ECSPI module instance number starting at 1.
//! @param tx_buf
//! @param rx_buf
//! @param bytes Number of bytes to transfer.
//!
//! @todo Use interrupts to get notification of transfer completion instead of
//!     polling the ECSPI STATREG and pausing 500 µs.
static int ecspi_xfer_mst(unsigned instance, const uint8_t * tx_buf, uint8_t * rx_buf, int bytes)
{
    uint32_t val;
    t_timer tmr;
    //количество полных 32-битных слов
    int wrds  = bytes >> 2;
    //количество байт при чтении/записи первого слова
    int first = bytes & 0x3;
    int i;

    // Start burst 
    HW_ECSPI_CONREG_SET(instance, BM_ECSPI_CONREG_SMC);

    // Write to Tx FIFO 
    val = 0;

    if (first)
    {
        if (tx_buf)
        {
            for (i=0; i < first; i++)
            {
                val <<= 8;
                val |= *tx_buf++;
            }
        }
        HW_ECSPI_TXDATA_WR(instance, val);
    }

    for (i=0; i < wrds; i++)
    {
        if (tx_buf)
        {
            val = *tx_buf++ << 24;
            val |= *tx_buf++ << 16;
            val |= *tx_buf++ << 8;
            val |= *tx_buf++;
        }
        HW_ECSPI_TXDATA_WR(instance, val);
    }

    // Wait for transfer complete 
    timer_set(&tmr, SPI_RETRY_DEAY*CLOCK_CONF_SECOND/1000);
    while (!HW_ECSPI_STATREG(instance).B.TC)
    {
        if (timer_expired(&tmr))
        {
#if DEBUG
            printf("ecspi_xfer: Transfer timeout.\n");
#endif
            return ERROR_GENERIC;
        }
    }




    // Read from Rx FIFO 
    if (first)
    {
        val = HW_ECSPI_RXDATA_RD(instance);
        if (rx_buf)
        {
            for (i=first-1; i >= 0; i--)
            {
                rx_buf[i] = val & 0xFF;
                val >>= 8;
            }
            rx_buf+=first;
        }
    }

    for (i=0 ; i < wrds; i++)
    {
        val = HW_ECSPI_RXDATA_RD(instance);
        if (rx_buf)
        {
            *rx_buf++ = (val >> 24) & 0xFF;
            *rx_buf++ = (val >> 16) & 0xFF;
            *rx_buf++ = (val >> 8) & 0xFF;
            *rx_buf++ = val & 0xFF;
        }
    }

    // Clear status 
    HW_ECSPI_STATREG_WR(instance, BM_ECSPI_STATREG_TC);

    return SUCCESS;
}

int ecspi_configure(dev_ecspi_e instance, const param_ecspi_t * param)
{
    // Reset eCSPI controller 
    HW_ECSPI_CONREG(instance).B.EN = 0;

    // Setup chip select 
    HW_ECSPI_CONREG(instance).B.CHANNEL_SELECT = param->channel;

    // Setup mode 
    uint32_t channelMask = 1 << param->channel;
    uint32_t value = HW_ECSPI_CONREG(instance).B.CHANNEL_MODE;
    BW_ECSPI_CONREG_CHANNEL_MODE(instance, param->mode ? (value | channelMask) : (value & ~channelMask));

    // Setup pre & post clock divider 
    HW_ECSPI_CONREG(instance).B.PRE_DIVIDER = (param->pre_div == 0) ? 0 : (param->pre_div - 1);
    HW_ECSPI_CONREG(instance).B.POST_DIVIDER = param->post_div;

    // Enable eCSPI 
    HW_ECSPI_CONREG(instance).B.EN = 1;

    // Setup SCLK_PHA, SCLK_POL, SS_POL 
    value = HW_ECSPI_CONFIGREG(instance).B.SCLK_PHA;
    HW_ECSPI_CONFIGREG(instance).B.SCLK_PHA = param->sclk_pha ? (value | channelMask) : (value & ~channelMask);
    
    value = HW_ECSPI_CONFIGREG(instance).B.SCLK_POL;
    HW_ECSPI_CONFIGREG(instance).B.SCLK_POL = param->sclk_pol ? (value | channelMask) : (value & ~channelMask);
    
    value = HW_ECSPI_CONFIGREG(instance).B.SS_POL;
    HW_ECSPI_CONFIGREG(instance).B.SS_POL = param->ss_pol ? (value | channelMask) : (value & ~channelMask);
    
    HW_ECSPI_CONFIGREG(instance).B.SS_CTL |= channelMask;
    
    return SUCCESS;
}

//! @todo Validate @a dev value for the chip, since not all chips will have all 5 instances.
int ecspi_open(dev_ecspi_e dev, const param_ecspi_t * param)
{
    // Configure IO signals 
    //ecspi_iomux_config(dev);
    
    // Ungate the module clock.
    clock_gating_config(REGS_ECSPI_BASE(dev), CLOCK_ON);

    // Configure eCSPI registers 
    ecspi_configure(dev, param);

    return SUCCESS;
}

int ecspi_close(dev_ecspi_e dev)
{
    // Disable controller 
    HW_ECSPI_CONREG(dev).B.EN = 0;
    
    // Gate the module clock.
    clock_gating_config(REGS_ECSPI_BASE(dev), CLOCK_OFF);

    return SUCCESS;
}

//! @todo Handle tranfers larger than the FIFO length!
int ecspi_xfer(dev_ecspi_e dev, const uint8_t * tx_buf, uint8_t * rx_buf, uint16_t bytes)
{
    uint32_t retv = SUCCESS;

    // Check burst length 
    if (bytes > ECSPI_MAX_BLOCK_SIZE)
    {
#if DEBUG
        printf("ecspi_xfer: Burst out of length.\n");
#endif
        retv = ERROR_GENERIC;
    }

    if (retv == SUCCESS)
    {
        // Prepare transfer 
        ecspi_start_transfer(dev, bytes*8);

        // Initiate transfer  
        uint32_t channel = HW_ECSPI_CONREG(dev).B.CHANNEL_SELECT;

        // Handle different mode transfer 
        if ((HW_ECSPI_CONREG(dev).B.CHANNEL_MODE & (1 << channel)) == 0)
        {
            retv = ecspi_xfer_slv(dev, tx_buf, rx_buf, bytes);
        }
        else
        {
            retv = ecspi_xfer_mst(dev, tx_buf, rx_buf, bytes);
        }
    }

    return retv;
}

////////////////////////////////////////////////////////////////////////////////
// EOF
////////////////////////////////////////////////////////////////////////////////
