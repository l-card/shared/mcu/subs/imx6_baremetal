#ifndef SPI_FLASH_H
#define SPI_FLASH_H

#include "ecspi_ifc.h"

typedef struct
{
    uint8_t cmd;
    uint32_t size;
    uint32_t time;
} t_spiflash_erase_cmd;

typedef struct
{
    uint8_t dummy_bytes;
    uint32_t max_freq;
} t_spiflash_fastread;


typedef struct
{
    dev_ecspi_e dev;

    param_ecspi_t spi_params;

    struct
    {
        uint8_t manufacturer;
        uint8_t type;
        uint8_t capacity;
    } JEDECID;
    uint32_t capacity;

    const t_spiflash_erase_cmd *erase_info;
    const t_spiflash_fastread *fast_rd;


    uint8_t cmd_buf[ECSPI_MAX_BLOCK_SIZE];

} t_spiflash;




int spiflash_open(t_spiflash *flash, dev_ecspi_e dev, uint8_t cs, uint32_t clk);
int spiflash_close(t_spiflash *flash);
int spiflash_read(t_spiflash *flash, uint32_t addr, uint8_t *rx_buf, uint32_t size);
int spiflash_erase(t_spiflash *flash, uint32_t addr, uint32_t size);
int spiflash_write(t_spiflash *flash, uint32_t addr, const uint8_t *wr_buf, uint32_t size);

#endif // SPI_FLASH_H
