#include <string.h>


#include "spi/spi_flash.h"
#include "core/ccm_pll.h"
#include "error_codes.h"

/* коды команд */
#define FLASH_CMD_CODE_PAGE_PROGRAM  0x02
#define FLASH_CMD_CODE_WRITE_ENABLE  0x06
#define FLASH_CMD_CODE_WRITE_DISABLE 0x04
#define FLASH_CMD_CODE_READ_STATUS   0x05
#define FLASH_CMD_CODE_READ          0x03
#define FLASH_CMD_CODE_READ_JEDEC_ID 0x9F

#define FLASH_CMD_CODE_FAST_READ     0x0B

#define FLASH_CMD_CODE_ERASE_4K      0x20
#define FLASH_CMD_CODE_ERASE_32K     0x52  //нет в micron
#define FLASH_CMD_CODE_ERASE_64K     0xD8

/* фиксированный полный размер команды */
#define FLASH_CMD_SIZE_READ_JEDEC_ID    4

#define FLASH_CMD_WITH_ADDR_SIZE        4

/* размер блока для записи с помощью PAGE_PROGRAM */
#define WRITE_BLOCK_SIZE                256

#define PAGE_PROGRAM_TOUT               5

/* набор команд для стирания для flash-памяти от Micron N25Q064A11 */
static const t_spiflash_erase_cmd f_erase_info_N25Q064A11[] =
{
    {FLASH_CMD_CODE_ERASE_64K, 64*1024, 3000},
    {FLASH_CMD_CODE_ERASE_4K,  4*1024,  800},
    {0,0}
};

static const t_spiflash_fastread f_fastrd_info_N25Q064A11 = {
    1, 30000000
};

/* получить подходящую команду для стирания по адресу и размеру */
static const t_spiflash_erase_cmd  * f_get_erase_cmd(t_spiflash *flash, uint32_t addr, uint32_t size)
{
    const t_spiflash_erase_cmd *fnd_cmd=0;
    for (const t_spiflash_erase_cmd *cmd=flash->erase_info; !fnd_cmd && cmd->cmd; cmd++)
    {
        if (!(addr & (cmd->size-1)) && (size >= cmd->size))
            fnd_cmd = cmd;
    }
    return fnd_cmd;
}

/* заполнение кода команды с 3-байтовым адресом */
static inline void f_fill_cmd_addr(t_spiflash *flash, uint8_t cmd, uint32_t addr)
{
    flash->cmd_buf[0] = cmd;
    flash->cmd_buf[1] = (addr >> 16) & 0xFF;
    flash->cmd_buf[2] = (addr >> 8) & 0xFF;
    flash->cmd_buf[3] = addr & 0xFF;
}

static int f_write_enable(t_spiflash *flash)
{
    flash->cmd_buf[0] = FLASH_CMD_CODE_WRITE_ENABLE;
    return ecspi_xfer(flash->dev, flash->cmd_buf, 0, 1);
}

static inline int f_read_status_reg(t_spiflash *flash, uint8_t *val)
{
    int err = 0;
    flash->cmd_buf[0] = FLASH_CMD_CODE_READ_STATUS;

    err = ecspi_xfer(flash->dev, flash->cmd_buf, flash->cmd_buf, 2);
    if (!err && val)
    {
        *val = flash->cmd_buf[1];
    }
    return err;
}

static int f_wait_rdy(t_spiflash *flash, uint32_t tout)
{
    int err = 0;
    int rdy = 0;

    while (!rdy && !err)
    {
        uint8_t status;
        err = f_read_status_reg(flash, &status);
        if (!err)
            rdy = !(status & 1);

        /** @todo timeout */
    }
    return err;
}

static inline int f_fill_clk(param_ecspi_t *params, uint32_t clk)
{
    int err = 0;
    int clk_div = (get_peri_clock(SPI_CLK)+clk-1)/clk;
    if (clk_div > 16)
        err = ERROR_INVALID_PARAMETER;

    if (!err)
    {
        if (clk_div==0)
            clk_div = 1;

        params->pre_div = clk_div;
        params->post_div = 0;
    }
    return err;
}


int spiflash_open(t_spiflash *flash, dev_ecspi_e dev, uint8_t cs, uint32_t clk)
{
    int err = (flash == NULL) || (cs >= 4) ? ERROR_INVALID_PARAMETER : SUCCESS;
    if (!err)
    {
        flash->spi_params.channel = cs;
        flash->spi_params.mode = 1; //master;
        flash->spi_params.ss_pol = 0;
        flash->spi_params.sclk_pol = 0;
        flash->spi_params.sclk_pha = 0;

        err = f_fill_clk(&flash->spi_params, clk);

        if (!err)
        {
            flash->dev = dev;
            err = ecspi_open(flash->dev, &flash->spi_params);
        }
    }

    if (!err)
    {
        flash->cmd_buf[0] = FLASH_CMD_CODE_READ_JEDEC_ID;
        err = ecspi_xfer(flash->dev, flash->cmd_buf, flash->cmd_buf, FLASH_CMD_SIZE_READ_JEDEC_ID);
        if (!err)
        {
            flash->JEDECID.manufacturer = flash->cmd_buf[1];
            flash->JEDECID.type = flash->cmd_buf[2];
            flash->JEDECID.capacity = flash->cmd_buf[3];

            if (flash->JEDECID.manufacturer==0x00)
            {
                err = ERROR_DEVICE_NOT_PRESENT;
            }
            else
            {
                flash->capacity = (1 << flash->JEDECID.capacity);

                flash->erase_info = f_erase_info_N25Q064A11;
                flash->fast_rd = 0;// &f_fastrd_info_N25Q064A11;
            }
        }
    }
    return err;
}

int spiflash_close(t_spiflash *flash)
{
    int err = flash == NULL ? ERROR_INVALID_PARAMETER : SUCCESS;
    if (!err)
        err = ecspi_close(flash->dev);
    return err;
}



int spiflash_read(t_spiflash *flash, uint32_t addr, uint8_t *rx_buf, uint32_t size)
{
    int err = flash == NULL ? ERROR_INVALID_PARAMETER : SUCCESS;
    if (!err)
    {
        uint8_t code=FLASH_CMD_CODE_READ;
        uint8_t dummy=0;

        if (flash->fast_rd)
        {
            param_ecspi_t par = flash->spi_params;
            f_fill_clk(&par, flash->fast_rd->max_freq);
            code = FLASH_CMD_CODE_FAST_READ;
            dummy = flash->fast_rd->dummy_bytes;
            ecspi_configure(flash->dev, &par);
        }


        while (size && !err)
        {
            int block_size = size;
            if (block_size > (ECSPI_MAX_BLOCK_SIZE-FLASH_CMD_WITH_ADDR_SIZE-dummy))
                block_size = ECSPI_MAX_BLOCK_SIZE-FLASH_CMD_WITH_ADDR_SIZE-dummy;
            f_fill_cmd_addr(flash, code, addr);

            err = ecspi_xfer(flash->dev, flash->cmd_buf, flash->cmd_buf, FLASH_CMD_WITH_ADDR_SIZE + block_size + dummy);
            if (!err)
            {
                memcpy(rx_buf, &flash->cmd_buf[FLASH_CMD_WITH_ADDR_SIZE+dummy], block_size);
                //printf("first rd = 0x%x, %x, %x, %x\n", rx_buf[0], rx_buf[1], rx_buf[2], rx_buf[3]);
                size-=block_size;
                rx_buf+=block_size;
                addr+=block_size;
            }

        }


        if (flash->fast_rd)
        {
            ecspi_configure(flash->dev, &flash->spi_params);
        }
    }
    return err;
}

int spiflash_erase(t_spiflash *flash, uint32_t addr, uint32_t size)
{
    int err = flash ==  NULL || (addr + size > flash->capacity) ? ERROR_INVALID_PARAMETER : 0;

    while (!err && size)
    {
        /* подбираем наиболее подходящую команду для стирания для данных размеров и адреса */
        const t_spiflash_erase_cmd *cmd = f_get_erase_cmd(flash, addr, size);
        if (cmd)
        {
            err = f_write_enable(flash);
            if (!err)
            {
                f_fill_cmd_addr(flash, cmd->cmd, addr);
                err = ecspi_xfer(flash->dev, flash->cmd_buf, flash->cmd_buf, 4);
            }

            if (!err)
                err = f_wait_rdy(flash, cmd->time);

            if (!err)
            {
                addr+=cmd->size;
                size-=cmd->size;
            }
        }
        else
        {
            err = ERROR_UNALIGNED_ADDR;
        }
    }

    return err;
}

int spiflash_write(t_spiflash *flash, uint32_t addr, const uint8_t *wr_buf, uint32_t size)
{
    int err = 0;
    if (!err)
    {
        while (size && !err)
        {
            /* размер за раз не превышает размера от текущего адреса до конца блока записи */
            uint32_t block_size = WRITE_BLOCK_SIZE - (addr & (WRITE_BLOCK_SIZE-1));
            if (block_size > size)
                block_size = size;
            if (block_size > (ECSPI_MAX_BLOCK_SIZE-FLASH_CMD_WITH_ADDR_SIZE))
                block_size = ECSPI_MAX_BLOCK_SIZE-FLASH_CMD_WITH_ADDR_SIZE;

            err = f_write_enable(flash);
            if (!err)
            {
                f_fill_cmd_addr(flash, FLASH_CMD_CODE_PAGE_PROGRAM, addr);
                memcpy(&flash->cmd_buf[FLASH_CMD_WITH_ADDR_SIZE], wr_buf, block_size);
                err = ecspi_xfer(flash->dev, flash->cmd_buf, 0, FLASH_CMD_WITH_ADDR_SIZE+block_size);
            }

            if (!err)
                err = f_wait_rdy(flash, PAGE_PROGRAM_TOUT);

            if (!err)
            {
                wr_buf+=block_size;
                addr += block_size;
                size-=block_size;
            }
        }
    }
    return err;
}
