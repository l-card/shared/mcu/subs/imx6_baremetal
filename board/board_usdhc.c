#include "registers/regsiomuxc.h"
#include "registers/regsusdhc.h"
#include "gpio/gpio.h"
#include <stdint.h>


#define USDHC3_POW_EN_PIN  1,29
#define USDHC3_CD_PIN  6,14

////////////////////////////////////////////////////////////////////////////////
// Code
////////////////////////////////////////////////////////////////////////////////



/*!
  * Set up the GPIO for USDHC
  */
void usdhc_gpio_config(uint32_t instance)
{
    if (instance == HW_USDHC3)
    {
        gpio_set_gpio(USDHC3_POW_EN_PIN);
        gpio_set_direction(USDHC3_POW_EN_PIN, GPIO_GDIR_OUTPUT);
        gpio_set_level(USDHC3_POW_EN_PIN, 1);

        gpio_set_gpio(USDHC3_CD_PIN);
        gpio_set_direction(USDHC3_CD_PIN, GPIO_GDIR_INPUT);
    }
}

int usdhc_card_detected(uint32_t instance)
{
    int ret = 1;
    if (instance == HW_USDHC3)
    {
        ret = !gpio_get_direction(USDHC3_CD_PIN);
    }
    return ret;
}

int usdhc_write_protected(uint32_t instance)
{
    return 0;
}
