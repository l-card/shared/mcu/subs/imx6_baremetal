#ifndef ERROR_CODES_H
#define ERROR_CODES_H

#include <stdio.h>
#include <stdarg.h>

#ifndef TRUE
    #define TRUE 1
#endif

#ifndef FALSE
    #define FALSE 0
#endif

#define SUCCESS                     (0)
#define ERROR_GENERIC               (-1)
#define ERROR_OUT_OF_MEMORY         (-2)
#define ERROR_INVALID_PARAMETER     (-3)
#define ERROR_DEVICE_NOT_PRESENT    (-4)
#define ERROR_UNSUPPORTED_DEVICE    (-5)
#define ERROR_UNALIGNED_ADDR        (-6)
#define ERROR_IMAGE_NOT_PRESENT     (-7)
#define ERROR_WRITE_VERIFY          (-8)



static inline void debug_printf(const char * format, ...)
{
#if defined(DEBUG)
    va_list args;
    va_start(args, format);
    vprintf(format, args);
    va_end(args);
#endif
}


#endif // ERROR_CODES_H
